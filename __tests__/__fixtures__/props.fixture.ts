export const initialContentProps: any = {
  id: "foo",
};

export const finalContentProps: any = {
  id: "bar",
};

export const dynamicMixBemProps: any = {
  _links: {
    self: {
      href: "https://localhost/api/",
    },
  },
  meta: { block: "exceptional" },
};

export const dynamicProps: any = {
  meta: {
    block: "exceptional",
    elem: "exceptionalElem",
    cls: "h-card",
    tag: "div",
    bem: false,
    js: true,
    mods: { name: "index" },
    elemMods: { type: "search" },
  },
};
