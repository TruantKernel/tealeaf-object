import BemJson, {
  isBemJson,
  BemJsonAttribute,
  BemJsonModifiers,
  BemJsonAttributes,
} from "./bem-json";
import { JSONPath } from "jsonpath-plus";
import { HALOLinks } from "hal-object/lib/cjs/halo";
import DynamicBemJson, {
  isBemJsonPath,
  BemJsonPath,
  DynamicBemJsonContent,
  DynamicBemJsonModifiers,
  DynamicBemJsonAttributes,
} from "./dynamic-bem-json";

export type BemNodeProps = {
  data: any;
  _links?: HALOLinks;
};

export default class BemNode {
  private readonly bem: DynamicBemJson;
  private props: {
    data: any;
    _links?: HALOLinks;
  };

  public constructor(
    bem: DynamicBemJson,
    props?: {
      data: any;
      _links?: HALOLinks;
    }
  ) {
    this.bem = bem;
    this.props = props ? props : { data: {} };
  }

  public setProps(props: { data: any; _links?: HALOLinks }) {
    this.props = props;
  }

  public getProps(): BemNodeProps {
    return this.props;
  }

  public resolve(): DynamicBemJson {
    return BemNode.resolvePaths(this.bem, this.props);
  }

  private static resolvePath(path: BemJsonPath, props: any): any {
    return JSONPath({
      path: path["$ref"] as string,
      json: props,
    })[0];
  }

  private static resolvePaths(bem: DynamicBemJson, props: any): BemJson {
    const res: BemJson = {};
    if (bem.attributes != null) {
      res.attributes = BemNode.resolveAttributes(bem.attributes, props);
    }
    if (bem.block != null) {
      res.block = BemNode.simpleResolve(bem.block, props) as string;
    }
    if (bem.elem != null) {
      res.elem = BemNode.simpleResolve(bem.elem, props) as string;
    }
    if (bem.cls != null) {
      res.cls = BemNode.simpleResolve(bem.cls, props) as string;
    }
    if (bem.tag != null) {
      res.tag = isBemJsonPath(bem.tag)
        ? BemNode.resolvePath(bem.tag, props)
        : bem.tag;
    }
    if (bem.bem != null) {
      res.bem = isBemJsonPath(bem.bem)
        ? BemNode.resolvePath(bem.bem, props)
        : bem.bem;
    }
    if (bem.js != null) {
      res.js = isBemJsonPath(bem.js)
        ? BemNode.resolvePath(bem.js, props)
        : bem.js;
    }
    if (bem.mods != null) {
      res.mods = BemNode.resolveBemJsonModifiers(bem.mods, props);
    }
    if (bem.elemMods != null) {
      res.elemMods = BemNode.resolveBemJsonModifiers(bem.elemMods, props);
    }
    if (bem.mix != null) {
      res.mix = BemNode.resolveBemJsonContentPaths(bem.mix, props);
    }
    if (bem.content != null) {
      res.content = BemNode.resolveBemJsonContentPaths(bem.content, props);
    }
    return res;
  }

  private static resolveBemJsonContentPaths(
    target: DynamicBemJsonContent,
    props: any
  ): BemJson | Array<BemJson | string> | string {
    let res: BemJson | Array<BemJson | string> | string;
    if (Array.isArray(target)) {
      res = new Array<BemJson>();
      for (let i = 0; i < target.length; i++) {
        res[i] = BemNode.resolveBemJsonContentPath(target[i], props);
      }
    } else {
      res = BemNode.resolveBemJsonContentPath(target, props);
    }
    return res;
  }

  private static simpleResolve(
    src: string | BemJsonPath,
    props: any
  ): string | boolean {
    return isBemJsonPath(src)
      ? (BemNode.resolvePath(src, props) as string)
      : src;
  }

  private static resolveAttributes(
    attributes: DynamicBemJsonAttributes,
    props: any
  ): BemJsonAttributes {
    const res: BemJsonAttributes = {};
    for (const property in attributes) {
      const attribute = (attributes as any)[property];
      if (attribute.constructor == Object) {
        res[property] = {};
        for (const innerProperty in attribute) {
          if (innerProperty == "$ref") {
            res[property] = BemNode.resolvePath(attribute, props);
          } else {
            (res[property] as BemJsonAttribute)[innerProperty] = (attributes[
              property
            ] as BemJsonAttribute)[innerProperty];
          }
        }
      } else {
        res[property] = attributes[property] as BemJsonAttribute;
      }
    }
    return res;
  }

  private static resolveBemJsonModifiers(
    mods: DynamicBemJsonModifiers,
    props: any
  ): BemJsonModifiers {
    let res: BemJsonModifiers = {};
    if (isBemJsonPath(mods)) {
      res = BemNode.resolvePath(mods, props) as BemJsonModifiers;
    } else {
      for (const property in mods) {
        const mod = (mods as any)[property];
        if (mod.constructor == Object) {
          res[property] = BemNode.resolvePath(mod, props);
        } else {
          res[property] = mods[property] as string | boolean;
        }
      }
    }
    return res;
  }

  private static resolveBemJsonContentPath(
    target: DynamicBemJsonContent,
    props: any
  ): BemJson | string {
    let res: BemJson | string = {};
    if (isBemJson(target)) {
      res = BemNode.resolvePaths(target as DynamicBemJson, props) as BemJson;
    } else if (isBemJsonPath(target)) {
      res = BemNode.resolvePath(target as BemJsonPath, props);
    } else {
      res = target as string;
    }
    return res;
  }
}
