import BemJson, {
  BemJsonAttribute,
  BemJsonObjectAttribute,
  BemJsonAttributes,
  BemJsonContent,
} from "./bem-json";

export type DynamicBemJsonAttributes = {
  [name: string]: DynamicBemJsonAttribute | BemJsonAttribute;
};
export type DynamicBemJsonAttribute =
  | string
  | {
      [name: string]:
        | DynamicBemJsonObjectAttribute
        | BemJsonObjectAttribute
        | string
        | BemJsonPath;
    }
  | BemJsonPath;
export type DynamicBemJsonObjectAttribute = {
  [name: string]: string | BemJsonPath;
};
export type BemJsonPath = { $ref: string };
export type DynamicBemJsonContent =
  | Array<DynamicBemJson | BemJson | string | BemJsonPath>
  | BemJsonPath
  | DynamicBemJson
  | BemJson
  | string;
export type DynamicBemJsonModifiers =
  | {
      [name: string]: string | boolean | BemJsonPath;
    }
  | BemJsonPath;

export function isBemJsonPath(test: any): test is BemJsonPath {
  return test.$ref ? true : false;
}

export default interface DynamicBemJson {
  block?: string | BemJsonPath;
  elem?: string | BemJsonPath;
  mods?: DynamicBemJsonModifiers;
  elemMods?: DynamicBemJsonModifiers;
  content?: DynamicBemJsonContent | BemJsonContent;
  mix?: DynamicBemJsonContent | BemJsonContent;
  js?: boolean | Record<string, unknown> | BemJsonPath;
  bem?: boolean | BemJsonPath;
  attributes?: DynamicBemJsonAttributes | BemJsonAttributes;
  cls?: string | BemJsonPath;
  tag?: boolean | string | BemJsonPath;
}
