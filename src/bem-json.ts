export type BemJsonAttributes = { [name: string]: BemJsonAttribute | string };
export type BemJsonAttribute = {
  [name: string]: BemJsonObjectAttribute | string;
};
export type BemJsonObjectAttribute = { [name: string]: string };
export type BemJsonContent = Array<BemJson | string> | BemJson | string;
export type BemJsonModifiers = {
  [name: string]: string | boolean;
};

export function isBemJson(test: any): test is BemJson {
  return test.block || test.elem ? true : false;
}

export default interface BemJson {
  block?: string;
  elem?: string;
  mods?: BemJsonModifiers;
  elemMods?: BemJsonModifiers;
  content?: BemJsonContent;
  mix?: BemJsonContent;
  js?: boolean | Record<string, unknown>;
  bem?: boolean;
  attributes?: BemJsonAttributes;
  cls?: string;
  tag?: boolean | string;
}
