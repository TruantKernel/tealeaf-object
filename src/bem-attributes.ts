export default interface BemAttributes {
  attributes: { [name: string]: string | { [name: string]: string } };
}
